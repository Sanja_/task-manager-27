/*
package ru.karamyshev.taskmanager.endpoint;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.karamyshev.taskmanager.api.endpoint.ITaskEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.bootstrap.Bootstrap;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.Task;
import ru.karamyshev.taskmanager.marker.ServerTestCategory;

import java.util.List;

@Category(ServerTestCategory.class)
public class TaskEndpointTest {

    private final IServiceLocator serviceLocator = new Bootstrap();

    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(serviceLocator);

    @Test
    public void createNameTaskTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        Assert.assertNotNull(session);
        taskEndpoint.createName(session, "task-1");
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), 1);
    }

    @Test
    public void createDescriptionTaskTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        Assert.assertNotNull(session);
        taskEndpoint.createDescription(session, "task-1", "task-1");
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), 1);
    }

    @Test
    public void addTaskTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        Assert.assertNotNull(session);
        final Task task = new Task();
        task.setName("task-1");
        task.setId(111111L);
        taskEndpoint.addTask(session, task);
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), 1);
    }

    @Test
    public void getTaskListTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        taskEndpoint.createName(session, "task-1");
        taskEndpoint.createName(session, "task-2");
        taskEndpoint.createName(session, "task-3");
        Assert.assertNotNull(session);
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), 3);
    }

    @Test
    public void findAllTaskTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        taskEndpoint.createName(session, "task-1");
        taskEndpoint.createName(session, "task-2");
        taskEndpoint.createName(session, "task-3");
        Assert.assertEquals(taskEndpoint.findAllTask(session).size(), 3);
    }

    @Test
    public void findOneTaskByIndexTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        taskEndpoint.createName(session, "task-1");
        final Task task = taskEndpoint.findTaskOneByIndex(session, 1);
        Assert.assertNotNull(task);
    }

    @Test
    public void findOneTaskByNameTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        taskEndpoint.createName(session, "task-1");
        final List<Task> tasks = taskEndpoint.findTaskOneByName(session, "task-1");
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findOneTaskByIdTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        taskEndpoint.createName(session, "task-1");
        final List<Task> tasks = taskEndpoint.findTaskOneByName(session, "task-1");
        final Task task = taskEndpoint.findTaskOneById(session, String.valueOf(tasks.get(0).getId()));
        Assert.assertNotNull(task);
    }


    @Test
    public void clearTaskTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        taskEndpoint.createName(session, "task-1");
        taskEndpoint.createName(session, "task-2");
        taskEndpoint.createName(session, "task-3");
        Assert.assertEquals(taskEndpoint.findAllTask(session).size(), 3);
        taskEndpoint.clearTask(session);
        Assert.assertEquals(taskEndpoint.findAllTask(session).size(), 0);
    }

    @Test
    public void updateTaskByIdTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        taskEndpoint.createName(session, "task-1");
        final Task task = serviceLocator.getTaskService().findOneByIndex(session.getUserId(), 1);
        Assert.assertNotNull(task);
        final Task taskUpdate = taskEndpoint.updateTaskById(session, String.valueOf(task.getId()),
                "user-11", "user-11");
        Assert.assertEquals(taskUpdate.getName(), "user-11");
    }

    @Test
    public void updateTaskByIndexTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        taskEndpoint.createName(session, "task-1");
        final Task task = serviceLocator.getTaskService().findOneByIndex(session.getUserId(), 1);
        Assert.assertNotNull(task);
        final Task taskUpdate = taskEndpoint.updateTaskByIndex(session, 1,
                "user-11", "user-11");
        Assert.assertNotNull(taskUpdate);
        Assert.assertEquals(taskUpdate.getName(), "user-11");
    }

    @Test
    public void removeTaskTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        taskEndpoint.createName(session, "task-1");
        taskEndpoint.createName(session, "task-2");
        taskEndpoint.createName(session, "task-3");
        final List<Task> tasks = taskEndpoint.findTaskOneByName(session, "task-1");
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), 3);
        Assert.assertEquals(tasks.size(), 1);
        taskEndpoint.removeTask(session, tasks.get(0));
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), 2);
    }

    @Test
    public void removeOneTaskByIndexTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        taskEndpoint.createName(session, "task-1");
        taskEndpoint.createName(session, "task-2");
        taskEndpoint.createName(session, "task-3");
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), 3);
        taskEndpoint.removeTaskOneByIndex(session, 1);
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), 2);
    }

    @Test
    public void removeOneTaskByNameTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        taskEndpoint.createName(session, "task-1");
        taskEndpoint.createName(session, "task-2");
        taskEndpoint.createName(session, "task-3");
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), 3);
        taskEndpoint.removeTaskOneByName(session, "task-1");
        Assert.assertEquals(taskEndpoint.findTaskOneByName(session, "task-1").size(), 0);
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), 2);
    }

    @Test
    public void removeOneTaskByIdTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        taskEndpoint.createName(session, "task-1");
        taskEndpoint.createName(session, "task-2");
        taskEndpoint.createName(session, "task-3");
        final Task task = serviceLocator.getTaskService()
                .findOneByIndex(session.getUserId(), 1);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), 3);
        taskEndpoint.removeTaskOneById(session, String.valueOf(task.getId()));
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), 2);
    }

}
*/
