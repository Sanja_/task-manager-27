/*
package ru.karamyshev.taskmanager.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.karamyshev.taskmanager.api.repository.IUserRepository;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.marker.ServerTestCategory;

import java.util.List;

@Category(ServerTestCategory.class)
public class UserRepositoryTest {

    private final IUserRepository userRepository = new UserRepository();

    private final static User user1 = new User();

    private final static User user2 = new User();

    private final static User user3 = new User();

    @BeforeClass
    public static void init() {
        user1.setLogin("User1");
        user1.setEmail("User1@com");
        user1.setRole(Role.USER);

        user2.setLogin("User2");
        user2.setEmail("User2@com");
        user2.setRole(Role.USER);

        user3.setLogin("User3");
        user3.setEmail("User3@com");
        user3.setRole(Role.ADMIN);
    }

    @Before
    public void addTest() {
        userRepository.add(user1);
        userRepository.add(user2);
        userRepository.add(user3);
    }

    @Test
    public void findAllTest() {
        final List<User> result = userRepository.findAll();
        Assert.assertNotNull(result);
        Assert.assertEquals(result.size(), 3);
    }

    @Test
    public void findByIdTest() {
        String userId = String.valueOf(user1.getId());
        Assert.assertNotNull(userId);
        final User user = userRepository.findById(userId);
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getId(), user1.getId());
    }

    @Test
    public void findByLoginTest() {
        final User user = userRepository.findByLogin(user1.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), user1.getLogin());
    }

    @Test
    public void removeByIdTest() {
        final List<User> result = userRepository.findAll();
        Assert.assertNotNull(result);
        final String userId = String.valueOf(user1.getId());
        userRepository.removeById(userId);
        Assert.assertEquals(result.size(), 2);
    }

    @Test
    public void removeByLogin() {
        final List<User> result = userRepository.findAll();
        Assert.assertNotNull(result);
        userRepository.removeByLogin(user1.getLogin());
        Assert.assertEquals(result.size(), 2);
    }

}
*/
