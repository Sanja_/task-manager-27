/*
package ru.karamyshev.taskmanager.repository;

import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.marker.ServerTestCategory;

import java.util.List;

@Category(ServerTestCategory.class)
public class ProjectRepositoryTest {

    private final static IProjectRepository projectRepository = new ProjectRepository();

    private final static Project project1 = new Project();

    private final static Project project2 = new Project();

    private final static Project project3 = new Project();

    @BeforeClass
    public static void init() {
        project1.setName("Project-test-1");
        project2.setName("Project-test-2");
        project3.setName("Project-test-3");
    }

    @Before
    public void addProjectTest() {
        projectRepository.add("1111111111", project1);
        projectRepository.add("1111111111", project2);
        projectRepository.add("3333333333", project3);
        Assert.assertEquals(projectRepository.findAll().size(), 3);
    }

    @After
    public void clearTest() {
        projectRepository.clear();
    }


    @Test
    public void findAllTest() {
        final List<Project> result = projectRepository.findAll(project1.getUserId());
        Assert.assertNotNull(result);
        Assert.assertEquals(result.size(), 2);
    }


    @Test
    public void findOneByIdTest() {
        final String projectId = String.valueOf(project1.getId());
        final Project result = projectRepository.findOneById(project1.getUserId(), projectId);
        Assert.assertNotNull(result);
        Assert.assertEquals(result.getName(), project1.getName());
    }

    @Test
    public void removeOneByIdTest() throws Exception {
        projectRepository.findAll(project1.getUserId());
        final String projectId = String.valueOf(project1.getId());
        projectRepository.removeOneById(project1.getUserId(), projectId);
        Assert.assertEquals(projectRepository.findAll(project1.getUserId()).size(), 1);
    }

    @Test
    public void findOneByIndexTest() {
        final Project result = projectRepository.findOneByIndex(project1.getUserId(), 0);
        Assert.assertNotNull(result);
        Assert.assertEquals(result.getName(), project1.getName());
    }

    @Test
    public void findOneByNameTest() {
        final List<Project> result = projectRepository.findOneByName(project1.getUserId(), project1.getName());
        Assert.assertNotNull(result);
        Assert.assertEquals(result.size(), 1);
        Assert.assertEquals(result.get(0).getName(), project1.getName());
    }

    @Test
    public void removeOneByIndexTest() {
        final List<Project> result = projectRepository.findAll(project1.getUserId());
        Assert.assertNotNull(result);
        projectRepository.removeOneByIndex(project1.getUserId(), 1);
        Assert.assertEquals(projectRepository.findAll(project1.getUserId()).size(), 1);
    }

    @Test
    public void removeOneByNameTest() {
        final List<Project> result = projectRepository.findAll(project1.getUserId());
        Assert.assertNotNull(result);
        projectRepository.removeOneByName(project1.getUserId(), project1.getName());
        Assert.assertEquals(projectRepository.findAll(project1.getUserId()).size(), 1);
    }

}
*/
