/*
package ru.karamyshev.taskmanager.service;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.karamyshev.taskmanager.api.repository.ITaskRepository;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.entity.Task;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.marker.ServerTestCategory;
import ru.karamyshev.taskmanager.repository.TaskRepository;

import java.util.List;

@Category(ServerTestCategory.class)
public class TaskServiceTest {

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private static User user1 = new User();

    @AfterClass
    public static void init() {
        user1.setId(1111L);
        user1.setLogin("user1");
        user1.setRole(Role.USER);
    }

    @Test
    public void createTest() throws Exception {
        final List<Task> taskList = taskRepository.findAll();
        Assert.assertEquals(taskList.size(), 0);
        final String userId = String.valueOf(user1.getId());
        taskService.create(userId, "task-1");
        Assert.assertEquals(taskRepository.findAll().size(), 1);
    }

    @Test
    public void createTaskDescriptionTest() throws Exception {
        final List<Task> taskList = taskRepository.findAll();
        Assert.assertEquals(taskList.size(), 0);
        final String userId = String.valueOf(user1.getId());
        taskService.create(userId, "task-1", "task-1");
        Assert.assertEquals(taskRepository.findAll().size(), 1);
    }

    @Test
    public void addTest() {
        final Task task = new Task();
        task.setId(111L);
        task.setName("task-1");
        final String userId = String.valueOf(user1.getId());
        taskService.add(userId, task);
        Assert.assertEquals(taskRepository.getList().size(), 1);
    }

    @Test
    public void removeTest() throws Exception {
        final String userId = String.valueOf(user1.getId());
        taskService.create(userId, "task-1");
        Assert.assertEquals(taskRepository.getList().size(), 1);
        final Task task = taskRepository.findOneByIndex(userId, 0);
        taskService.remove(userId, task);
        Assert.assertEquals(taskRepository.getList().size(), 0);
    }

    @Test
    public void findAll() throws Exception {
        final String userId = String.valueOf(user1.getId());
        taskService.create(userId, "task-1");
        taskService.create(userId, "task-2");
        taskService.create(userId, "task-3");
        Assert.assertEquals(taskRepository.getList().size(), 3);
        final List<Task> taskList = taskService.findAll(userId);
        Assert.assertEquals(taskList.size(), 3);
    }

    @Test
    public void clearTest() {
        final String userId = String.valueOf(user1.getId());
        taskService.create(userId, "task-1");
        taskService.create(userId, "task-2");
        taskService.create(userId, "task-3");
        Assert.assertEquals(taskRepository.getList().size(), 3);
        taskService.clear(userId);
        Assert.assertEquals(taskRepository.getList().size(), 0);
    }

    @Test
    public void findOneByIndexTest() throws Exception {
        final String userId = String.valueOf(user1.getId());
        final Task task = new Task();
        task.setName("task-1");
        task.setId(111);
        taskService.add(userId, task);
        final Task tempTask = taskService.findOneByIndex(userId, 1);
        Assert.assertEquals(task, tempTask);
    }

    @Test
    public void findOneByNameTest() throws Exception {
        final String userId = String.valueOf(user1.getId());
        final Task task = new Task();
        task.setName("task-1");
        task.setId(111);
        taskService.add(userId, task);
        final List<Task> tempTask = taskService.findOneByName(userId, task.getName());
        for (Task task1 : tempTask) Assert.assertEquals(task.getName(), task1.getName());
    }

    @Test
    public void removeOneByIndexTest() {
        final String userId = String.valueOf(user1.getId());
        taskService.create(userId, "task-1");
        taskService.create(userId, "task-2");
        taskService.create(userId, "task-3");
        Assert.assertEquals(taskService.getList().size(), 3);
        taskService.removeOneByIndex(userId, 1);
        Assert.assertEquals(taskService.getList().size(), 2);
    }

    @Test
    public void removeOneByNameTest() {
        final String userId = String.valueOf(user1.getId());
        taskService.create(userId, "task-1");
        taskService.create(userId, "task-2");
        taskService.create(userId, "task-3");
        Assert.assertEquals(taskService.getList().size(), 3);
        taskService.removeOneByName(userId, "task-2");
        Assert.assertEquals(taskService.getList().size(), 2);
    }

    @Test
    public void removeOneByIdTest() {
        final String userId = String.valueOf(user1.getId());
        final Task task1 = new Task();
        final Task task2 = new Task();
        final Task task3 = new Task();
        task1.setName("task-1");
        task1.setId(111);
        task2.setName("task-2");
        task2.setId(222);
        task3.setName("task3");
        task3.setId(333);
        taskService.add(userId, task1);
        taskService.add(userId, task2);
        taskService.add(userId, task3);

        Assert.assertEquals(taskService.getList().size(), 3);
        taskService.removeOneById(userId, String.valueOf(task3.getId()));
        Assert.assertEquals(taskService.getList().size(), 2);
    }

    @Test
    public void updateTaskByIndexTest() {
        final String userId = String.valueOf(user1.getId());
        final Task task = new Task();
        task.setName("task-1");
        task.setId(111);
        task.setDescription("111");
        taskService.add(userId, task);
        taskService.updateTaskByIndex(userId, 1,
                "task-2", "222");
        final Task tempTask = taskService.findOneById(userId, "111");
        Assert.assertNotNull(tempTask);
        Assert.assertEquals(tempTask.getId(), 111);
        Assert.assertEquals(tempTask.getName(), "task-2");
        Assert.assertEquals(tempTask.getDescription(), "222");
    }

    @Test
    public void updateTaskByIdTest() {
        final String userId = String.valueOf(user1.getId());
        final Task task = new Task();
        task.setName("task-1");
        task.setId(111);
        task.setDescription("111");
        taskService.add(userId, task);
        taskService.updateTaskById(userId, String.valueOf(task.getId()),
                "task-2", "222");
        final Task tempTask = taskService.findOneById(userId, "111");
        Assert.assertNotNull(tempTask);
        Assert.assertEquals(tempTask.getId(), 111);
        Assert.assertEquals(tempTask.getName(), "task-2");
        Assert.assertEquals(tempTask.getDescription(), "222");
    }

}
*/
