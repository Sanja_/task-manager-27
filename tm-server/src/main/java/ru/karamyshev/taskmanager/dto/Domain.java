package ru.karamyshev.taskmanager.dto;

import lombok.Getter;
import lombok.Setter;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.entity.Task;
import ru.karamyshev.taskmanager.entity.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement
public class  Domain implements Serializable {

    private List<ProjectDTO> projects = new ArrayList<>();

    private List<TaskDTO> tasks = new ArrayList<>();

    private List<UserDTO> users = new ArrayList<>();

}
