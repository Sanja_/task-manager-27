package ru.karamyshev.taskmanager.exception;

public class NotMatchPasswordsException extends RuntimeException {

    public NotMatchPasswordsException() {
        super("Error! Old Password not match...");
    }

}
