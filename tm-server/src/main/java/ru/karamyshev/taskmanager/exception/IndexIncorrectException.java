package ru.karamyshev.taskmanager.exception;

public class IndexIncorrectException extends RuntimeException {

    public IndexIncorrectException(final Throwable cause) {
        super(cause);
    }

    public IndexIncorrectException(final String value) {
        super("Error! This value ``" +value+"`` is not number.");
    }

    public IndexIncorrectException() {

        super("Error! Index is  incorrect...");
    }

}
