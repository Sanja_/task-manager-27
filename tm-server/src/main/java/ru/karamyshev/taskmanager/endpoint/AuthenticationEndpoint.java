/*
package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.endpoint.IAuthenticationEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AuthenticationEndpoint implements IAuthenticationEndpoint {

    private IServiceLocator serviceLocator;

    public AuthenticationEndpoint() {
    }

    public AuthenticationEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public void login(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    ) {
        serviceLocator.getAuthenticationService().login(login, password);
    }

    @Nullable
    @Override
    @WebMethod
    public String getUserId(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getAuthenticationService().getUserId();
    }

    @Nullable
    @Override
    @WebMethod
    public void checkRoles(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "role", partName = "role") @Nullable Role[] roles
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAuthenticationService().checkRoles(roles);
    }

    @Nullable
    @Override
    @WebMethod
    public String getCurrentLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getAuthenticationService().getCurrentLogin();
    }

    @Override
    @WebMethod
    public boolean isAuth() {
        return serviceLocator.getAuthenticationService().isAuth();
    }

    @Override
    @WebMethod
    public void registryUser(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    ) throws Exception {
        serviceLocator.getAuthenticationService().registry(login, password, email);
    }

    @Override
    @WebMethod
    public void renameLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "newLogin", partName = "newLogin") @Nullable String newLogin
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        final User currentUser = serviceLocator.getUserService().findById(session.getUserId());
        serviceLocator.getAuthenticationService().renameLogin(session.getUserId(), currentUser.getLogin(), newLogin);
    }

    @Nullable
    @Override
    @WebMethod
    public User showProfile(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getAuthenticationService().showProfile(session.getUserId(), login);
    }

    @Override
    @WebMethod
    public void renamePassword(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "oldPassword", partName = "oldPassword") @Nullable final String oldPassword,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        final User currentUser = serviceLocator.getUserService().findById(session.getUserId());
        serviceLocator.getAuthenticationService()
                .renamePassword(session.getUserId(), currentUser.getLogin(), oldPassword, newPassword);
    }

    @Override
    @WebMethod
    public void logout() {
        serviceLocator.getAuthenticationService().logout();
    }

}
*/
