package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.util.Date;

@WebService
public class Calculator {

    @Nullable
    @WebMethod
    public int sum(
            @WebParam(name = "a") int a,
            @WebParam(name = "b") int b
    ) {
        return a + b;
    }

    @Nullable
    public Date getData() {
        return new Date();
    }

    @Nullable
    public User getUser() {
        final User user = new User();
        user.setFirstName("Санёчек");
        user.setLastName("Санин");
        user.setMiddleName("Саныч");
        user.setLogin("sanja");
        user.setRole(Role.ADMIN);
        user.setEmail("1@1.ru");
        return user;
    }

    public static void main(String[] args) {
        final Calculator calculator = new Calculator();
        Endpoint.publish("http://localhost:1234/Calculator?wsdl", calculator);
    }

}
