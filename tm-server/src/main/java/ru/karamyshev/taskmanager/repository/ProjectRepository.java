package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.entity.Task;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {


    @NotNull
    private EntityManager entityManager;

    public ProjectRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final Project project) {
        entityManager.persist(project);
    }

    @Nullable
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) {
        @Nullable final TypedQuery<Project> typedQuery = entityManager.createQuery(
                "FROM Project WHERE user_id = :userId", Project.class);
        typedQuery.setParameter("userId", userId);
        if (typedQuery == null) return null;
        return typedQuery.getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        @Nullable final TypedQuery<Project> typedQuery = entityManager.createQuery(
                "WHERE FROM Project", Project.class);
        if (typedQuery == null) return null;
        return typedQuery.getResultList();
    }

    @Override
    public void clear(final @NotNull String userId) {
        entityManager.createQuery(
                "DELETE Project WHERE user_id = :userId")
                .setParameter("userId",userId)
                .executeUpdate();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final TypedQuery<Project> typedQuery = entityManager.createQuery(
                "FROM Project WHERE user_id = :userId AND id = :id", Project.class);
        typedQuery.setMaxResults(1);
        typedQuery.setParameter("userId", userId);
        typedQuery.setParameter("id", id);
        if (typedQuery == null) return null;
        return typedQuery.getSingleResult();
    }

    @Nullable
    @Override
    public Project removeOneById(@NotNull final String userId, @NotNull final String id) {
        final Project project = findOneById(userId, id);
        if (project == null) return null;
        entityManager.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final TypedQuery<Project> typedQuery = entityManager.createQuery(
                "FROM Project WHERE user_id = :userId AND name = :name", Project.class);
        typedQuery.setMaxResults(1);
        typedQuery.setParameter("userId", userId);
        typedQuery.setParameter("name", name);
        if (typedQuery == null) return null;
        return typedQuery.getSingleResult();
    }

    @Nullable
    @Override
    public Project removeOneByName(@NotNull final String userId, @NotNull final String name) {
        final Project project = findOneByName(userId, name);
        if (project == null) return null;
        entityManager.remove(project);
        return project;
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Project", Project.class)
                .executeUpdate();
    }

}
