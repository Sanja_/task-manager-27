package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.ITaskRepository;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.dto.TaskDTO;
import ru.karamyshev.taskmanager.entity.Task;
import ru.karamyshev.taskmanager.exception.empty.EmptyUserIdException;
import ru.karamyshev.taskmanager.exception.empty.IdEmptyException;
import ru.karamyshev.taskmanager.exception.empty.NameEmptyException;
import ru.karamyshev.taskmanager.repository.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    public TaskService(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String taskName,
            @Nullable final String projectName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskName == null || taskName.isEmpty()) throw new NameEmptyException();
        if (projectName == null || projectName.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task = new Task();
        task.setName(taskName);
        task.setUser(serviceLocator.getUserService().findById(userId));
        task.setProject(serviceLocator.getProjectService().findOneByName(userId, projectName));

        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();

        @Nullable final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.add(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String taskName,
            @Nullable final String projectName,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskName == null || taskName.isEmpty()) throw new NameEmptyException();
        if (projectName == null || projectName.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new Exception();

        @Nullable final Task task = new Task();
        task.setName(taskName);
        task.setUser(serviceLocator.getUserService().findById(userId));
        task.setProject(serviceLocator.getProjectService().findOneByName(userId, projectName));
        task.setDescription(description);

        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();

        @Nullable final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.add(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final Task task) {
        if (task == null) return;
        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();

        try {
            entityManager.getTransaction().begin();
            entityManager.remove(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        @Nullable final List<Task> taskDTOList = taskRepository.findAllByUserId(userId);
        entityManager.close();
        return TaskDTO.toDTO(taskDTOList);
    }

    public void clearTaskByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();

        @Nullable final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task findOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final Task task = taskRepository.findOneByName(userId, name);
        entityManager.close();
        return task;
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final Task task = taskRepository.removeOneById(userId, id);
        entityManager.close();
        return task;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll() {
        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<Task> taskList = taskRepository.findAll();
        entityManager.close();
        return TaskDTO.toDTO(taskList);
    }

    @Nullable
    @Override
    public void updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new Exception();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) return;
        task.setName(name);
        task.setDescription(description);

        @NotNull final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();


        try {
            entityManager.getTransaction().begin();
            entityManager.merge(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();

        @Nullable final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeOneByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();

        @Nullable final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public void removeAll() {
        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();

        @Nullable final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeAll();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

}
