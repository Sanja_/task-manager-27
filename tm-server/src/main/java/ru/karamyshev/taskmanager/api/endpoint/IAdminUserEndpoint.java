package ru.karamyshev.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.dto.SessionDTO;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminUserEndpoint {

    @WebMethod
    void createUserWithEmail(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    ) throws Exception;

    @WebMethod
    void createUserWithRole(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "login", partName = "login") String login,
            @WebParam(name = "password", partName = "password") String password,
            @WebParam(name = "role", partName = "role") Role role
    ) throws Exception;

    @WebMethod
    void lockUser(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "login", partName = "login") String login
    ) throws Exception;

    @WebMethod
    void unlockUser(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "login", partName = "login") String login
    ) throws Exception;

    @Nullable
    @WebMethod
    void removeUsByLog(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "login", partName = "login") String login
    ) throws Exception;

}
