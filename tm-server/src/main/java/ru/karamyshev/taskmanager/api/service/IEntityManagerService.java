package ru.karamyshev.taskmanager.api.service;

import javax.persistence.EntityManager;

public interface IEntityManagerService {

    EntityManager getEntityManager();

}
