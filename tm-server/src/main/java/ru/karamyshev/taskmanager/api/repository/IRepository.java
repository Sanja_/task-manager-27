package ru.karamyshev.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @Nullable
    List<E> findAll();

    void clear();

    void add(@NotNull List<E> eList);

    void add(@NotNull E... e);

    void load(@NotNull List<E> eList);

}
