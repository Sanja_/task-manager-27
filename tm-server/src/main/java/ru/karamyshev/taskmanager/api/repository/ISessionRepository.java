package ru.karamyshev.taskmanager.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

   @Nullable Session add(@Nullable Session session);

    void remove(@Nullable Session session);

    @Nullable
    List<Session> findByUserId(@Nullable String userId) throws Exception;

    @Nullable
    Session findById(@Nullable String id) throws Exception;

    void removeByUserId(@Nullable String userId) throws Exception;

    boolean contains(@Nullable String id) throws Exception;
}
