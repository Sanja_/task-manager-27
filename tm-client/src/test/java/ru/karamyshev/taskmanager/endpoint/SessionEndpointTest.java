/*
package ru.karamyshev.taskmanager.endpoint;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.karamyshev.taskmanager.api.IServiceLocator;
import ru.karamyshev.taskmanager.bootstrap.Bootstrap;
import ru.karamyshev.taskmanager.marker.ClientTestCategory;

@Category(ClientTestCategory.class)
public class SessionEndpointTest {

    final IServiceLocator serviceLocator = new Bootstrap();

    final SessionEndpoint sessionEndpoint = serviceLocator.getSessionEndpoint();

    @Test
    public void openSessionTest() throws java.lang.Exception {
        Assert.assertNull(sessionEndpoint.openSession("testUser321", "testUser4141"));
        final Session session = sessionEndpoint.openSession("admin", "admin");
        Assert.assertNotNull(session);
        final User user = serviceLocator.getUserEndpoint().findUserByLogin(session, "admin");
        Assert.assertNotNull(user);
        sessionEndpoint.closeSession(session);
    }

    @Test
    public void closeSessionTest() throws java.lang.Exception {
        final Session session = sessionEndpoint.openSession("admin", "admin");
        Assert.assertNotNull(session);
        final User user = serviceLocator.getUserEndpoint().findUserByLogin(session, "admin");
        Assert.assertNotNull(user);
        final Result result = sessionEndpoint.closeSession(session);
        Assert.assertEquals(result.getClass().getSimpleName(), "Result");
    }

    @Test
    public void closeAllSessionTest() throws java.lang.Exception {
        serviceLocator.getUserEndpoint().createUser("user-1", "user-1");
        serviceLocator.getUserEndpoint().createUser("user-2", "user-2");
        final Session session1 = sessionEndpoint.openSession("user-1", "user-1");
        final Session session2 = sessionEndpoint.openSession("user-2", "user-2");
        final User user1 = serviceLocator.getUserEndpoint().findUserByLogin(session1, "user-1");
        final User user2 = serviceLocator.getUserEndpoint().findUserByLogin(session1, "user-2");
        Assert.assertNotNull(user1);
        Assert.assertNotNull(user2);
        final Result result = sessionEndpoint.closeAllSession(session1);
        Assert.assertEquals(result.getClass().getSimpleName(), "Result");
    }

}
*/
