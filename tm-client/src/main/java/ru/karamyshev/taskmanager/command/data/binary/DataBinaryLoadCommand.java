package ru.karamyshev.taskmanager.command.data.binary;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;

public class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String arg() {
        return "-dtbnld";
    }

    @NotNull
    @Override
    public String name() {
        return "data-bin-load";
    }

    @Override
    public @NotNull String description() {
        return "Save data to binary file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BINARY LOAD]");
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminEndpoint().loadDataBinary(session);
        System.out.println("[OK]");
        System.out.println("[YOU ARE LOGGED OUT]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
