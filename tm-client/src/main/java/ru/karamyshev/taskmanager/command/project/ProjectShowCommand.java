package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.*;

import java.util.List;

public class ProjectShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-prlst";
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public @NotNull String description() {
        return "Show project list.";
    }

    @Override
    public void execute() throws Exception_Exception {
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("[PROJECT LIST]");
        final List<ProjectDTO> projects = serviceLocator.getProjectEndpoint().findAllProject(session);
        int index = 1;
        for (ProjectDTO project : projects) {
            System.out.println(index + ". " + project.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
