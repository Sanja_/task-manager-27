package ru.karamyshev.taskmanager.command.info;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.api.ICommandService;
import ru.karamyshev.taskmanager.command.AbstractCommand;

import java.util.List;

public class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @Override
    public @NotNull String description() {
        return "Display terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("\n [HELP]");
        final ICommandService commandService = serviceLocator.getCommandService();
        final List<AbstractCommand> commands = commandService.getTerminalCommands();
        for (final AbstractCommand command : commands) {
            System.out.println(command.name() + " (" + command.arg() + ") : -" + command.description());
        }
        System.out.println("[OK]");
    }

}
