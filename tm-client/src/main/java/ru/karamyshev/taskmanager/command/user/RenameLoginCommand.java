package ru.karamyshev.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;
import ru.karamyshev.taskmanager.util.TerminalUtil;


public class RenameLoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "rnm-lgn";
    }

    @NotNull
    @Override
    public String name() {
        return "rename-login";
    }

    @Override
    public @NotNull String description() {
        return "Rename login account";
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("CHANGE ACCOUNT LOGIN");
        System.out.println("[ENTER NEW LOGIN]");
        final String newLogin = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().removeUserByLogin(session, newLogin);
        System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
