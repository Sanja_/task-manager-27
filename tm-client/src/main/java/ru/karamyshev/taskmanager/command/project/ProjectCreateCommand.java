package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-prtcrt";
    }

    @NotNull
    @Override
    public String name() {
        return "project-create";
    }

    @Override
    public @NotNull String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("[CREATE PROJECT");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectEndpoint().createNameProject(session, name, description);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
