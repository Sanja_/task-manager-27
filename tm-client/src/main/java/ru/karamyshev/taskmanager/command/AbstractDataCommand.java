package ru.karamyshev.taskmanager.command;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

public class AbstractDataCommand extends AbstractCommand implements Serializable {

    protected static final String FILE_BINARY = "./data.bin";
    protected static final String FILE_BASE64 = "./data.base64";
    protected static final String FILE_FAST_JSON = "./data-fast.json";
    protected static final String FILE_FAST_XML = "./data-fast.xml";
    protected static final String FILE_JAX_JSON = "./data-jax.json";
    protected static final String FILE_JAX_XML = "./data-jax.xml";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Nullable
    @Override
    public String name() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return null;
    }


    @Override
    @SneakyThrows
    public void execute() {
    }

}
