package ru.karamyshev.taskmanager.command;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.IServiceLocator;
import ru.karamyshev.taskmanager.endpoint.Role;


public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void execute() throws Exception;

}
