package ru.karamyshev.taskmanager;

import ru.karamyshev.taskmanager.bootstrap.Bootstrap;

public class App {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
