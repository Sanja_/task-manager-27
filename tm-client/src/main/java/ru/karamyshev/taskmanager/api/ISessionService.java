package ru.karamyshev.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;

public interface ISessionService {

    @Nullable
    void setSession(SessionDTO session);

    void clearSession();

    @Nullable
    SessionDTO getSession();

}
